const babel = require('gulp-babel');
const concat = require('gulp-concat');
const gulp = require('gulp');
const uglify = require('gulp-uglify');
const browserSync = require('browser-sync');
const plumber = require('gulp-plumber');
// const imagemin  = require('gulp-imagemin');
const sourcemaps = require('gulp-sourcemaps');
// const autoprefixer = require('gulp-autoprefixer');
const notify = require("gulp-notify");
const gutil = require('gulp-util');
const sass = require('gulp-dart-sass');
const pug = require('gulp-pug');
const htmlmin = require('gulp-htmlmin');
const newer = require('gulp-newer');

const server = browserSync.create();
const paths = {
	js: {
		src: ['develop/js/app.js', 'develop/custom_elements/**/*.js'],
		dest: 'public/js'
	},
	vendor: {
		src: 'develop/js/vendor/*.js',
		dest: 'public/js'
	},
	images: {
		src: ['develop/assets/images/*', 'develop/assets/svg/*'],
		dest: 'public/assets/images'
	},
	css: {
		src: 'develop/sass/style.scss',
		dest: 'public/css'
	},
	html: {
		src: ['develop/html/*.pug', 'develop/custom_elements/**/*.pug', 'develop/custom_elements/*.pug'],
		dest: 'public'
	},
	fonts: {
		src: 'develop/assets/fonts/*',
		dest: 'public/assets/fonts'
	}
};

var reportError = function (error) {
	var lineNumber = (error.lineNumber) ? 'LINE ' + error.lineNumber + ' -- ' : '';

	notify({
		title: 'Task Failed [' + error.plugin + ']',
		message: lineNumber + 'See console.',
		sound: 'Sosumi' // See: https://github.com/mikaelbr/node-notifier#all-notification-options-with-their-defaults
	}).write(error);
	var report = '';
	var chalk = gutil.colors.white.bgRed;

	report += chalk('TASK:') + ' [' + error.plugin + ']\n';
	report += chalk('PROB:') + ' ' + error.message + '\n';
	if (error.lineNumber) { report += chalk('LINE:') + ' ' + error.lineNumber + '\n'; }
	if (error.fileName)   { report += chalk('FILE:') + ' ' + error.fileName + '\n'; }
	console.error(report);

	this.emit('end');
}


//=====================================================

function html() {
	return gulp.src(paths.html.src)
		.pipe(pug())
		.pipe(concat('index.html'))
		.pipe(htmlmin({collapseWhitespace: true}))
		.pipe(gulp.dest(paths.html.dest))
		.pipe(browserSync.reload({stream:true}))
}

function fonts() {
	return gulp.src(paths.fonts.src)
		.pipe(gulp.dest(paths.fonts.dest))
		.pipe(browserSync.reload({stream:true}))
}

// Optimize Images
function images() {
	return gulp.src(paths.images.src)
		.pipe(newer(paths.images.dest))
		// .pipe(
		// 	imagemin([
		// 		imagemin.gifsicle({ interlaced: true }),
		// 		imagemin.jpegtran({ progressive: true }),
		// 		imagemin.optipng({ optimizationLevel: 5 }),
		// 		imagemin.svgo({
		// 			plugins: [
		// 				{
		// 					removeViewBox: false,
		// 					collapseGroups: true
		// 				}
		// 			]
		// 		})
		// 	])
		// )
		.pipe(gulp.dest(paths.images.dest))
		.pipe(browserSync.reload({stream:true}));
}

// CSS task
function css() {
	return gulp.src(paths.css.src)
		.pipe(sourcemaps.init())
		.pipe(plumber({
			errorHandler: reportError
		}))
		.pipe(sass({ outputStyle: "expanded" }))
		.on('error', reportError)
		// .pipe(autoprefixer())
		.pipe(sourcemaps.write())
		.pipe(gulp.dest(paths.css.dest))
		.pipe(browserSync.reload({stream:true}));
}

function js() {
	return gulp.src(paths.js.src, { sourcemaps: true })
		.pipe(plumber({
			errorHandler: reportError
		}))
		.pipe(babel())
		.on('error', reportError)
		.pipe(uglify())
		.pipe(concat('app.js'))
		.pipe(gulp.dest(paths.js.dest))
		.pipe(browserSync.reload({stream:true}));
}

function vendor() {
	return gulp.src(paths.vendor.src, { sourcemaps: true })
		.pipe(concat('vendor.min.js'))
		.pipe(gulp.dest(paths.vendor.dest))
		.pipe(browserSync.reload({stream:true}));
}

function reload(done) {
	server.reload();
	done();
}

function serve(done) {
	server.init({
		server: {
			baseDir: 'public'
		}
	});
	done();
}


//=====================================================

const watch = () => {
	gulp.watch(paths.js.src, gulp.series(js, reload));
	gulp.watch(paths.images.src, gulp.series(images, reload));
	gulp.watch(['develop/sass/*.scss', 'develop/sass/**/*.scss', 'develop/sass/**/**/*.scss', 'develop/custom_elements/**/*.scss'], gulp.series(css, reload));
	gulp.watch(paths.html.src, gulp.series(html, reload));

}

const dev = gulp.series(js, css, fonts, html, images, vendor, serve, watch);
gulp.task("default", dev);