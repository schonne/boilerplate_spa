'use strict';

// SET GLOBAL DATA OBJECT AND POPULATE FROM LOCALSTORAGE (IF AVAILABLE)
window.Global = window.Global || {};
if (localStorage.getItem("Global") !== null) {
	Global.data = JSON.parse(localStorage.getItem('Global'));
}

// SETUP FIREBASE AND LOG USER IN
// var config = {
//     apiKey: "",
//     authDomain: "",
//     databaseURL: "",
//     projectId: "",
//     storageBucket: "",
//     messagingSenderId: ""
// };
// firebase.initializeApp(config);
// firebase.auth().signInAnonymously();
// firebase.auth().onAuthStateChanged(firebaseUser => {
// 	Global.user = Global.user || {};
// 	if (firebaseUser) {
// 		Global.user.id = firebaseUser.uid;
// 		Global.user.isAnonymous = firebaseUser.isAnonymous;
// 	}
// });

document.addEventListener("DOMContentLoaded", function(event) {
	let referrer = document.referrer.replace("http://localhost:3000/", "").replace(/\/$/, "");
	// location.href.match(/([^\/]*)\/*$/)[1] <---last part of URL

	//REGISTER ROUTES
	Global.Router.addRoute('home', '<loading-element js-hook="loading-element"></loading-element>');
	if (referrer.length === 0) {
		Global.Router.start('home');
	} else {
		Global.Router.start(referrer);
	}
});