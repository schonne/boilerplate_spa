'use strict';

window.Global = window.Global || {};

(function () {

	Global.Router = function () {

		var routingTable = {};

		function addRoute(route, html) {
			routingTable[route] = html;
		}

		function start(route) {
			var state = { route: route };
			//window.history.replaceState(state, route, route);
			renderRoute(state);
		}

		function go(route, data) {
			var params = "";
			if (data) {
				data.forEach(function(key, value){
					params += "/" + value;
				});
			}

			var state = { route: route };
			document.getElementsByTagName("BODY")[0].setAttribute('page', route);
			//window.history.pushState(null, null, "/" + route + params);
			renderRoute(state);
		}

		function back() {
			window.history.back();
		}

		function renderRoute(state) {
			document.querySelector("main").innerHTML = routingTable[state.route];
		}

		window.onpopstate = function (event) {
			if (event.state) renderRoute(event.state);
			// PhantomJS doesn't always have event.state populated so the need for
			// the if statement
		};

		return { addRoute: addRoute, start: start, go: go, back: back };

	}();
})();