document.addEventListener("DOMContentLoaded", function(event) {
	customElements.define('loading-element', class extends HTMLElement {
		connectedCallback() {
			this.innerHTML = document.getElementById('loading').innerHTML;
		}
	});
});